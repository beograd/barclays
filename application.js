$(document).ready(function(){
    
    // placeholders
    var $inputElement = $(".meta-nav__search-input");
    var placeholder = $inputElement.attr('placeholder');
    
    $inputElement.val(placeholder);
    $inputElement.click(function(){
        if ($(this).val() === placeholder) {
            $(this).val('');
        }
    });
    
    $inputElement.blur(function(){
        if ($(this).val() === '') {
            $(this).val(placeholder);
        }
    });
    
    
    // tabs
    $(".tab__nav-item a").click(function(event){
        event.preventDefault();
        
        $(this).parent().parent().find('.tab__nav-item').removeClass('tab__nav-item--active');
        $(this).parent().addClass('tab__nav-item--active');
        
        var targetPanel = $(this).attr('href');
        $(this).parent().parent().parent().parent().find('.tab__panel').removeClass('tab__panel--active');
        $(targetPanel).addClass('tab__panel--active');
    });
    
    
    // accordions
    $(".accordion__title").click(function(event){
        event.preventDefault();
        
        var targetPanel = $(this).attr('href');
        var enabledPanel = $(this).parent().hasClass('accordion__item--open');

        $(".accordion__item").removeClass('accordion__item--open');
        if (!enabledPanel) {
            $(this).parent().addClass('accordion__item--open');
        }
    });
    
    // carousel slider
    $(".jcarousel-control-prev, .jcarousel-control-next").click(function(event){
        event.preventDefault();
    });
    
    $(".slider__content button").click(function(){
        var baseUrl = $("base").attr('href');
        var url = $(this).parent().parent().attr('href');
        if (!!url) {
            if (!!baseUrl) {
                url = baseUrl + url;
            }
            window.location = url;
        }
    });
    
    (function($) {
        $(function() {
            if ($('.jcarousel').length < 1) {
                return null;
            }
            
            $('.jcarousel').jcarousel();

            $('.jcarousel-control-prev')
                .on('jcarouselcontrol:active', function() {
                    $(this).removeClass('inactive');
                })
                .on('jcarouselcontrol:inactive', function() {
                    $(this).addClass('inactive');
                })
                .jcarouselControl({
                    target: '-=1'
                });

            $('.jcarousel-control-next')
                .on('jcarouselcontrol:active', function() {
                    $(this).removeClass('inactive');
                })
                .on('jcarouselcontrol:inactive', function() {
                    $(this).addClass('inactive');
                })
                .jcarouselControl({
                    target: '+=1'
                });

//            $('.jcarousel-pagination')
//                .on('jcarouselpagination:active', 'a', function() {
//                    $(this).addClass('active');
//                })
//                .on('jcarouselpagination:inactive', 'a', function() {
//                    $(this).removeClass('active');
//                })
//                .jcarouselPagination();
        });
    })(jQuery);
    
    
    // teaser fix
    function replaceAll(str, find, replace) {
      return str.replace(new RegExp(find, 'g'), replace);
    }
    
    
});